package main.java.nl.utwente.di.bookQuote;

public class Quoter {
    public double getBookPrice(String isbn) {
        int celsius = Integer.parseInt(isbn);
        double Fahrenheit = celsius * (9/5) + 32;
        return Fahrenheit;
    }
}
